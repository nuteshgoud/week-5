package week5task;

import java.util.Scanner;

public class Puzzle1 {
	static int row = 0, col = 0;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		char puzzle[][] = new char[5][5];

		for (int i = 0; i < 5; i++) {
			String str = sc.next();
			puzzle[i] = str.toCharArray();
		}

		/*
		 * char[][] puzzle = { { 't', 'r', 'g', 's', 'j' }, { 'x', 'd', 'o',
		 * 'k', 'i' }, { 'm', '_', 'v', 'l', 'n' }, { 'w', 'p', 'a', 'b', 'e' },
		 * { 'u', 'q', 'h', 'c', 'f' } };
		 */
		String string = "ARBLO";
		System.out.println("The input moves are " + string);
		System.out.println("The input puzzle is");
		doMoves(puzzle, string);
		displayPuzzle(puzzle);

		if (isGameOver(puzzle))
			System.out.println("END GAME");

		if (row == -1 && col == -1)
			System.out.println("No Configuration");

	}

	public static boolean isGameOver(char[][] puzzle) {

		String str1 = "";
		String str2 = "abcdefghijklmnopqrstuvwx";
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				str1 += puzzle[i][j];
			}
		}

		if (str1.equals(str2))
			return true;
		else
			return false;
	}

	public static void swap(char[][] puzzle, int row2, int col2) {
		char temp = puzzle[row][col];
		puzzle[row][col] = puzzle[row2][col2];
		puzzle[row2][col2] = temp;

	}

	public static void findEmpty(char[][] puz) {

		int i = 0;
		int j = 0;
		for (i = 0; i < 5; i++) {
			for (j = 0; j < 5; j++) {
				if (puz[i][j] == '_') {
					row = i;
					col = j;
				}
			}

		}
	}

	public static boolean isValidMove(char[][] puzzle, char move) {
		switch (move) {
		case 'A':
			if (row == 0)
				return false;
			swap(puzzle, row - 1, col);
			row--;
			return true;
		case 'B':
			if (row == 4)
				return false;
			swap(puzzle, row + 1, col);
			row++;
			return true;
		case 'L':
			if (col == 0)
				return false;
			swap(puzzle, row, col - 1);
			col--;
			return true;
		case 'R':
			if (col == 4)
				return false;
			swap(puzzle, row, col + 1);
			col++;
			return true;
		default:
			System.out.println("Not a valid move: '" + move + "'");
			return false;

		}
	}

	public static void displayPuzzle(char[][] puzzle) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print(puzzle[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void doMoves(char[][] puzzle, String str) {
		for (int i = 0; i < str.length(); i++) {
			if (isGameOver(puzzle)) {
				return;
			} else {
				char move = str.charAt(i);
				displayPuzzle(puzzle);
				System.out.println();
				System.out.println(move);
				findEmpty(puzzle);
				if (!isValidMove(puzzle, move)) {
					row = -1;
					col = -1;
					return;
				}
			}
		}
	}
}
